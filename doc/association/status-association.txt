EXEMPLE DE STATUTS1
Propos� aux associations d�clar�es par application de la
loi du 1er juillet 1901 et du d�cret du 16 ao�t 1901.

Cet exemple est � compl�ter et � adapter. Les commentaires en italiques et en bleu constituent une aide � la r�daction�; ils doivent �tre supprim�s avant enregistrement et/ou impression.

ARTICLE PREMIER - NOM

Il est fond� entre les adh�rents aux pr�sents statuts une association r�gie par la loi du 1er juillet 1901 et le d�cret du 16 ao�t 1901, ayant pour titre : .................................... 
Accorder de l�attention au choix du nom�; penser �ventuellement au nom d�usage (sigle, etc.)�; faire �ventuellement une recherche � l�INPI sur les noms prot�g�s/d�pos�s.

ARTICLE 2 - BUT OBJET

Cette association a pour objet ........ 
Pr�voir un objet assez large afin d'�viter une r�vision de statuts et lister les activit�s envisag�es pour atteindre l�objet. Pr�ciser �ventuellement � cet article (sinon � l�article ��ressources��) l�exercice d�activit�s �conomiques. En effet, une association dont l�objet ou les moyens d�action impliquent des activit�s �conomiques doit le mentionner express�ment dans ses statuts (Code de commerce Article L442-7).

ARTICLE 3 - SI�GE SOCIAL 

Le si�ge social est fix� �............
Le si�ge social d'une association peut-�tre fix� au domicile d'un des fondateurs, dans une mairie (demander autorisation pr�alable), etc. L�indication d�une localit� peut suffire mais complique les relations avec les tiers (b�n�ficiaires, administrations, banque, etc.).

Il pourra �tre transf�r� par simple d�cision du conseil d'administration ; 
Il peut �tre envisag� la ratification par l'assembl�e g�n�rale�

Article 4 - DUREE 
La dur�e de l�association est illimit�e.
Cette dur�e peut �tre fix�e � une date pr�cise ou faire r�f�rence � l�atteinte de l�objet d�fini � l�article 2.

ARTICLE 5 - COMPOSITION 

L'association se compose de :
a) Membres d'honneur
b) Membres bienfaiteurs
c) Membres actifs ou adh�rents
Pr�ciser la nature ou qualit� des membres pouvant adh�rer (personnes physiques ; personnes morales, sections, etc.) et, dans le cas de personnes morales, indiquer les modalit�s de repr�sentation dans les organes dirigeants.

ARTICLE 6 - ADMISSION 

Article optionnel.
L�association est ouverte � tous, sans condition ni distinction.
D�finir �ventuellement les�conditions � remplir�pour pouvoir adh�rer � l�association : conditions d��ge ou d�exp�rience dans un domaine, n�cessit� d��tre parrain�-e-, d�un agr�ment par l�un des organes (conseil d�administration, assembl�e g�n�rale, bureau), etc.�:
��Pour faire partie de l'association, il faut �tre agr�� par le conseil d�administration, qui statue, lors de chacune de ses r�unions, sur les demandes d'admission pr�sent�es.��
La libert� d�association se d�cline en deux aspects�: chacun a le droit d�adh�rer ou non � une association�; une association est libre de choisir ses adh�rents.

ARTICLE 7 - MEMBRES � COTISATIONS

Sont membres actifs ceux qui ont pris l'engagement de verser annuellement une somme de....� � titre de cotisation.
Sont membres d'honneur ceux qui ont rendu des services signal�s � l'association; ils sont dispens�s de cotisations; 
Sont membres bienfaiteurs, les personnes qui versent un droit d'entr�e de.....................�uros et une cotisation annuelle (de........�) fix�e chaque ann�e par l'assembl�e g�n�rale. 

Toute cotisation pourra �tre rachet�e moyennant le paiement d'une somme minima �gale � �.
Pr�ciser qui paie une cotisation et qui a le pouvoir de voter � l�assembl�e g�n�rale. Il peut �tre utile de stipuler que c�est l�assembl�e qui fixe le montant des cotisations dans le r�glement int�rieur afin d'�viter une r�vision fr�quente des statuts.

ARTICLE 8. - RADIATIONS 

La qualit� de membre se perd par :
a) La d�mission;
b) Le d�c�s;
c) La radiation prononc�e par le conseil d'administration pour non-paiement de la cotisation ou pour motif grave, l'int�ress� ayant �t� invit� (par lettre recommand�e) � fournir des explications devant le bureau et/ou par �crit. 
Pr�ciser les modalit�s de la radiation, les possibilit�s de d�fense et de recours du membre.
Les motifs graves peuvent �tre pr�cis�s ici ou dans le r�glement int�rieur.

ARTICLE 9. - AFFILIATION

La pr�sente association est affili�e � � et se conforme aux statuts et au r�glement int�rieur de cette f�d�ration (nom, logo, etc.).
Elle peut par ailleurs adh�rer � d�autres associations, unions ou regroupements par d�cision du conseil d�administration.

ARTICLE 10. - RESSOURCES 

Les ressources de l'association comprennent :
1� Le montant des droits d'entr�e et des cotisations;
2� Les subventions de l'Etat, des d�partements et des communes.
? Ne pas h�siter � pr�voir d'autres ressources si n�cessaire et/ou de r�diger ainsi cet article�:
3� Toutes les ressources autoris�es par les lois et r�glements en vigueur.�� 
Si cela n�a pas �t� fait � l�article 2, pr�ciser ici que l�association exercera des activit�s �conomiques et lesquelles (Code de commerce Article L442-7�: ��Aucune association (�) ne peut, de fa�on habituelle, offrir des produits � la vente, les vendre ou fournir des services si ces activit�s ne sont pas pr�vues par ses statuts��.)

ARTICLE 11 - ASSEMBLEE GENERALE ORDINAIRE 

L'assembl�e g�n�rale ordinaire comprend tous les membres de l'association � quelque titre qu'ils soient. Les statuts peuvent pr�voir que certains membres de l'association, par exemple qui ne versent qu'une cotisation tr�s faible, ne prennent pas part � l'assembl�e g�n�rale.

Elle se r�unit chaque ann�e au mois de.............. 

Quinze jours au moins avant la date fix�e, les membres de l'association sont convoqu�s par les soins du secr�taire. L'ordre du jour figure sur les convocations. 
Le pr�sident, assist� des membres du conseil, pr�side l'assembl�e et expose la situation morale ou l�activit� de l'association. 
Le tr�sorier rend compte de sa gestion et soumet les comptes annuels (bilan, compte de r�sultat et annexe) � l'approbation de l'assembl�e. 
L�assembl�e g�n�rale fixe le montant des cotisations annuelles et du droit d�entr�e � verser par les diff�rentes cat�gories de membres.
Ne peuvent �tre abord�s que les points inscrits � l'ordre du jour. 

Les d�cisions sont prises � la majorit� des voix des membres pr�sents ou repr�sent�s (ou des suffrages exprim�s).
Il est proc�d�, apr�s �puisement de l'ordre du jour, au renouvellement des membres sortants du conseil.
Il est prudent de fixer des conditions de quorum et de majorit� pour la validit� des d�lib�rations de l'assembl�e g�n�rale ordinaire
Pr�voir les r�gles de repr�sentation des membres absents si n�cessaire.

Toutes les d�lib�rations sont prises � main lev�e, except� l��lection des membres du conseil.
Les d�cisions des assembl�es g�n�rales s�imposent � tous les membres, y compris absents ou repr�sent�s.

ARTICLE 12 - ASSEMBLEE GENERALE EXTRAORDINAIRE 

Si besoin est, ou sur la demande de la moiti� plus un des membres inscrits, (ou par exemple � la demande d�un quart des membres) le pr�sident peut convoquer une assembl�e g�n�rale extraordinaire, suivant les modalit�s pr�vues aux pr�sents statuts et uniquement pour modification des statuts ou la dissolution ou pour des actes portant sur des immeubles.
Les modalit�s de convocation sont les m�mes que pour l�assembl�e g�n�rale ordinaire.
Les d�lib�rations sont prises � la majorit� (ou des deux tiers) des membres pr�sents (ou des suffrages exprim�s).


ARTICLE 13 - CONSEIL D'ADMINISTRATION 
La loi de 1901 n�impose pas l�existence d�un conseil d�administration ou d�un bureau. Ce n�est qu�un usage, pratique et tr�s r�pandu.

L'association est dirig�e par un conseil de X membres, �lus pour X ann�es par l'assembl�e g�n�rale. Les membres sont r��ligibles. 

Le conseil �tant renouvel� chaque ann�e par moiti�, la premi�re ann�e, les membres sortants sont d�sign�s par tirage au sort. 
Le renouvellement des membres du conseil par fraction est pr�f�rable.

En cas de vacances, le conseil pourvoit provisoirement au remplacement de ses membres. Il est proc�d� � leur remplacement d�finitif par la plus prochaine assembl�e g�n�rale. Les pouvoirs des membres ainsi �lus prennent fin � l'expiration le mandat des membres remplac�s. 
(Distinguer clairement les pr�rogatives de l�AG et du CA concernant par exemple les modalit�s de repr�sentation de l�association en justice, etc.)

Le conseil d'administration se r�unit au moins une fois tous les six mois, sur convocation du pr�sident, ou � la demande du quart de ses membres. Les r�unions du conseil peuvent �tre plus fr�quentes.

Les d�cisions sont prises � la majorit� des voix; en cas de partage, la voix du pr�sident est pr�pond�rante. 

Tout membre du conseil qui, sans excuse, n'aura pas assist� � trois r�unions cons�cutives sera consid�r� comme d�missionnaire. 

Le conseil d�administration peut d�l�guer tel ou tel de ses pouvoirs, pour une dur�e d�termin�e, � un ou plusieurs de ses membres (signature d�un bail des ch�ques, etc.).
Depuis 2011, des pr�cisions ont �t� apport�es, par un nouvel article 2 bis, � la loi du 1er juillet 1901 pour faciliter la cr�ation et la gestion d�une association par des mineurs. (T�l�charger la plaquette du minist�re charg� de la vie associative)


ARTICLE 14 � LE BUREAU 

Le conseil d'administration �lit parmi ses membres, (� bulletin secret�?), un bureau compos� de :
1) Un-e- pr�sident-e- ;
2) Un-e- ou plusieurs vice-pr�sident-e-s ;
3) Un-e- secr�taire et, s'il y a lieu, un-e- secr�taire adjoint-e- ;
4) Un-e- tr�sorier-e-, et, si besoin est, un-e- tr�sorier-e- adjoint-e-. 
Pour pr�venir des difficult�s fr�quentes, pr�ciser que les fonctions de pr�sident et de tr�sorier ne sont pas cumulables.
Pr�ciser, ici ou dans un r�glement int�rieur, les fonctions, attributions et pouvoirs respectifs des membres du bureau.

ARTICLE 15 � INDEMNITES

Toutes les fonctions, y compris celles des membres du conseil d�administration et du bureau, sont gratuites et b�n�voles. Seuls les frais occasionn�s par l�accomplissement de leur mandat sont rembours�s sur justificatifs. Le rapport financier pr�sent� � l�assembl�e g�n�rale ordinaire pr�sente, par b�n�ficiaire, les remboursements de frais de mission, de d�placement ou de repr�sentation.
Ces dispositions peuvent �tre affin�es dans un r�glement int�rieur (nature des frais, des missions, qualit� des b�n�ficiaires, etc.)

ARTICLE - 16 - REGLEMENT INTERIEUR 

Un r�glement int�rieur peut �tre �tabli par le conseil d'administration, qui le fait alors approuver par l'assembl�e g�n�rale. 

Ce r�glement �ventuel est destin� � fixer les divers points non pr�vus par les pr�sents statuts, notamment ceux qui ont trait � l'administration interne de l'association. 

ARTICLE - 17 - DISSOLUTION 

En cas de dissolution prononc�e selon les modalit�s pr�vues � l�article 12, un ou plusieurs liquidateurs sont nomm�s, et l'actif net, s'il y a lieu, est d�volu � un organisme ayant un but non lucratif (ou � une association ayant des buts similaires) conform�ment aux d�cisions de l�assembl�e g�n�rale extraordinaire qui statue sur la dissolution. L�actif net ne peut �tre d�volu � un membre de l�association, m�me partiellement, sauf reprise d�un apport. 
Ne pas interdire l�attribution de l�actif net � un membre pourrait compromettre le crit�re de gestion d�sint�ress�e, d�clinaison fiscale de l�article 1er de loi de 1901, et donc la qualification  d�int�r�t g�n�ral. 

Article � 18 LIBERALITES�:
Article � ins�rer  pour pouvoir accepter des legs -testaments- et des donations -entre vifs- (article 6 de la loi du 1er juillet 1901).

Le rapport et les comptes annuels, tels que d�finis � l�article 11 (y compris ceux des comit�s locaux) sont adress�s chaque ann�e au Pr�fet du d�partement.
L�association s�engage � pr�senter ses registres et pi�ces de comptabilit� sur toute r�quisition des autorit�s administratives en ce qui concerne l�emploi des lib�ralit�s qu�elle serait autoris�e � recevoir, � laisser visiter ses �tablissements par les repr�sentants de ces autorit�s comp�tents et � leur rendre compte du fonctionnement desdits �tablissements.


��Fait ��.., le�.�20.. �
Signatures de deux repr�sentants (nom, pr�nom et fonction) au minimum, n�cessaires pour la formalit� de d�claration de l�association.
